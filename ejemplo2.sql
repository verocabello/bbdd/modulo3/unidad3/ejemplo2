﻿-- Modulo 3 Unidad 3

-- EJEMPLO 2

-- 1
-- procedimiento almacenado que reciba un texto y un caracter. debe indicarse si ese caracter esta en el texto. utilizar locate

DELIMITER //
CREATE OR REPLACE PROCEDURE e1a(caracter char(1), texto varchar(100))
  BEGIN
    DECLARE resultado varchar(100) DEFAULT "el caracter no esta";
    IF(LOCATE(caracter,texto)>0) THEN
      SET resultado="el caracter esta";
    END IF;
    SELECT resultado;
  END //
DELIMITER;

CALL e1a('i','hola');


-- procedimiento almacenado que reciba un texto y un caracter. debe indicarse si ese caracter esta en el texto. utilizar position

DELIMITER //
CREATE OR REPLACE PROCEDURE e1b(caracter char(1), texto varchar(100))
  BEGIN
    DECLARE resultado varchar(100) DEFAULT "el caracter no esta";
    IF(POSITION(caracter IN texto)>0) THEN
      SET resultado="el caracter esta";
    END IF;
    SELECT resultado;
  END //
DELIMITER;

CALL e1b('j','ejemplos');


-- 2
-- procedimiento almacenado que reciba un texto y un caracter. debe indicar todo el texto que haya antes de la primera vez que aparece ese caracter

DELIMITER //
CREATE OR REPLACE PROCEDURE e2(texto varchar(100),caracter char(1))
  BEGIN
    DECLARE resultado varchar(100) DEFAULT NULL;
    SET resultado=SUBSTRING_INDEX(texto, caracter, 1);
    SELECT resultado;
  END //
DELIMITER;

CALL e2('escribir un texto','x');


-- 3
-- procedimiento almacenado que reciba 3 numeros, 2 argumentos de tipo salida y que devuelva el numero mas grande y el numero mas pequeño de los tres numeros pasados

DELIMITER //
CREATE OR REPLACE PROCEDURE e3(a int, b int, c int, OUT v1 int, OUT v2 int)
  BEGIN
    SET v1=(SELECT GREATEST(a,b,c));
    SET v2=(SELECT LEAST(a,b,c));
  END //
DELIMITER;

CALL e3(51,23,67,@a,@b);
SELECT @a,@b;


SELECT * FROM datos;


-- 4
-- procedimiento almacenado que muestre cuantos numeros1 y numeros2 son mayores que 50

DELIMITER //
CREATE OR REPLACE PROCEDURE e4()
  BEGIN
    DECLARE resultado int;
    DECLARE n1 int;
    DECLARE n2 int;
    SELECT COUNT(*) INTO n1 FROM datos WHERE numero1>50;
    SET n2=(SELECT COUNT(*) FROM datos WHERE numero2>50);
    SET resultado=n1+n2;
    SELECT resultado;
  END //
DELIMITER;

CALL e4();


SET @v=(SELECT COUNT(*) FROM datos);
SELECT @v;
SELECT COUNT(*) INTO @v FROM datos;
SELECT COUNT(*) INTO @v FROM datos WHERE numero1>50;

-- 5
-- procedimiento almacenado que calcule la suma y la resta de numero1 y numero2 de la tabla datos

DELIMITER //
CREATE OR REPLACE PROCEDURE e5()
  BEGIN
    UPDATE datos
      SET suma=numero1+numero2;
    UPDATE datos
      SET resta=numero1-numero2;
  END //
DELIMITER;

CALL e5();

-- 6
-- procedimiento almacenado que primiero ponga todos los valores de suma y resta a NULL y despues calcule la suma solamente si el numero1 es mayor que el numero2 y calcule la resta de numero2-numero1 si
-- el numero2 es mayor o numero1-numero2 si es mayor el numero 1

DELIMITER //
CREATE OR REPLACE PROCEDURE e6()
  BEGIN
   UPDATE datos
    SET suma=NULL;
   UPDATE datos
    SET resta=NULL;
   UPDATE datos
    SET suma=numero1+numero2
    WHERE numero1>numero2;
  UPDATE datos
    SET resta=numero2-numero1
    WHERE numero2>numero1;
  UPDATE datos
    SET resta=numero1-numero2
    WHERE numero1>numero2;
  END //
DELIMITER;

CALL e6();

-- 7
-- procedimiento almacenado que coloque en el campo junto el texto1, texto2

DELIMITER //
CREATE OR REPLACE PROCEDURE e7()
  BEGIN
   UPDATE datos
    SET junto=CONCAT(texto1," ",texto2);
  END //
DELIMITER;

CALL e7();

-- 8
-- procedimiento almacenado que coloque en el campo junto el valor de null. despues debe colocar en el campo junto el texto1-texto2 si el rango es A y si es rango B debe colocar texto1+texto2. si el rango
-- es distinto debe colocar texto1 nada mas

DELIMITER //
  CREATE OR REPLACE PROCEDURE e8()
    BEGIN
     UPDATE datos
      SET junto=NULL;
   UPDATE datos
    SET texto1=TRIM(texto1), texto2=TRIM(texto2);
     UPDATE datos
      SET junto=CONCAT(texto1," - ",texto2)
      WHERE rango="A";
     UPDATE datos
      SET junto=CONCAT(texto1," + ",texto2)
      WHERE rango="B";
     UPDATE datos
      SET junto=texto1
      WHERE rango<>"A" AND rango<>"B";
    END //
 DELIMITER;

CALL e8();


DELIMITER //
CREATE OR REPLACE PROCEDURE e8v2()
  BEGIN
   UPDATE datos
    SET junto=NULL;
   UPDATE datos
    SET texto1=TRIM(texto1), texto2=TRIM(texto2);
   UPDATE datos
    SET junto=IF(rango="A",CONCAT(texto1," - ",texto2),IF(rango="B",CONCAT(texto1," + ",texto2),texto1));
  END //
DELIMITER;

CALL e8v2();









SELECT * FROM datos;

